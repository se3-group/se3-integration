package wwi18b2.se3.group8.se3integration.service;

import com.fasterxml.jackson.databind.util.ArrayIterator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import wwi18b2.se3.group8.se3integration.model.MoviePreference;
import wwi18b2.se3.group8.se3integration.repository.MoviePreferenceRepository;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@ExtendWith(MockitoExtension.class)
public class MoviePreferenceServiceUnitTest {

    @Mock
    MoviePreferenceRepository moviePreferenceRepository;

    @Autowired
    @InjectMocks
    MoviePreferenceService moviePreferenceService;

    @Test
    public void getLikedMoviesForFriends(){
        MoviePreference moviePreference1 = generateMoviePreference(1L, 1L, "1");
        MoviePreference moviePreference2 = generateMoviePreference(2L, 2L, "1");
        MoviePreference[] moviePreferencesArray1 = {moviePreference1, moviePreference2};
        Iterable<MoviePreference> moviePreferences1 = new ArrayIterator<>(moviePreferencesArray1);

        MoviePreference moviePreference3 = generateMoviePreference(3L, 1L, "2");
        MoviePreference moviePreference4 = generateMoviePreference(4L, 2L, "2");
        MoviePreference[] moviePreferencesArray2 = {moviePreference3, moviePreference4};
        Iterable<MoviePreference> moviePreferences2 = new ArrayIterator<>(moviePreferencesArray2);

        Mockito.when(moviePreferenceRepository.findAllByCustomerIdAndLikeMovieTrue("1")).thenReturn(moviePreferences1);
        Mockito.when(moviePreferenceRepository.findAllByCustomerIdAndLikeMovieTrue("2")).thenReturn(moviePreferences2);

        List<Long> likedMovies = moviePreferenceService.getTogetherLikedMoviesByCustomerIds("1" ,"2");
        assertThat(likedMovies, hasItem(1L));
        assertThat(likedMovies, hasItem(2L));
        assertThat(likedMovies, hasSize(2));
    }

    @Test
    public void getLikedMoviesForFriendsDoesNotReturnMoviesLikedFromOnePerson(){
        MoviePreference moviePreference1 = generateMoviePreference(1L, 1L, "1");
        MoviePreference moviePreference2 = generateMoviePreference(2L, 2L, "1");
        MoviePreference[] moviePreferencesArray1 = {moviePreference1, moviePreference2};
        Iterable<MoviePreference> moviePreferences1 = new ArrayIterator<>(moviePreferencesArray1);

        MoviePreference moviePreference3 = generateMoviePreference(3L, 3L, "2");
        MoviePreference moviePreference4 = generateMoviePreference(4L, 4L, "2");
        MoviePreference[] moviePreferencesArray2 = {moviePreference3, moviePreference4};
        Iterable<MoviePreference> moviePreferences2 = new ArrayIterator<>(moviePreferencesArray2);

        Mockito.when(moviePreferenceRepository.findAllByCustomerIdAndLikeMovieTrue("1")).thenReturn(moviePreferences1);
        Mockito.when(moviePreferenceRepository.findAllByCustomerIdAndLikeMovieTrue("1")).thenReturn(moviePreferences2);

        List<Long> likedMovies = moviePreferenceService.getTogetherLikedMoviesByCustomerIds("1" ,"2");
        assertThat(likedMovies, hasSize(0));
    }

    @Test
    public void getLikedMoviesForFriendsReturnsEmptyForEmptyLists(){
        MoviePreference[] moviePreferencesArray1 = {};
        Iterable<MoviePreference> moviePreferences1 = new ArrayIterator<>(moviePreferencesArray1);

        MoviePreference[] moviePreferencesArray2 = {};
        Iterable<MoviePreference> moviePreferences2 = new ArrayIterator<>(moviePreferencesArray2);

        Mockito.when(moviePreferenceRepository.findAllByCustomerIdAndLikeMovieTrue("1")).thenReturn(moviePreferences1);
        Mockito.when(moviePreferenceRepository.findAllByCustomerIdAndLikeMovieTrue("2")).thenReturn(moviePreferences2);

        List<Long> likedMovies = moviePreferenceService.getTogetherLikedMoviesByCustomerIds("1" ,"2");
        assertThat(likedMovies, hasSize(0));
    }

    private MoviePreference generateMoviePreference(Long id, Long movieId, String customerId){
        return new MoviePreference(id, movieId, customerId, true);
    }
}
