package wwi18b2.se3.group8.se3integration;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource(
		locations = "classpath:application-test.properties")
class Se3IntegrationApplicationTests {

	@Test
	void contextLoads() {
	}

}
