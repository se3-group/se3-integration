package wwi18b2.se3.group8.se3integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Se3IntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(Se3IntegrationApplication.class, args);
	}

}
