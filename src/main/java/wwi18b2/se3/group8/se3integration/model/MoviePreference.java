package wwi18b2.se3.group8.se3integration.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MoviePreference {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long id;

    Long movieId;

    String customerId;

    boolean likeMovie;
}
