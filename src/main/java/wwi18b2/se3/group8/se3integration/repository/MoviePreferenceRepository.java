package wwi18b2.se3.group8.se3integration.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import wwi18b2.se3.group8.se3integration.model.MoviePreference;

public interface MoviePreferenceRepository extends PagingAndSortingRepository<MoviePreference, Long> {
    Iterable<MoviePreference> findAllByCustomerIdAndLikeMovieTrue(String customerId);

    Iterable<MoviePreference> findAllByCustomerId(String customerId);

    MoviePreference findByCustomerIdAndMovieId(String customerId, Long movieId);
}