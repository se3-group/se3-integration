package wwi18b2.se3.group8.se3integration.controller;

import org.springframework.web.bind.annotation.*;
import wwi18b2.se3.group8.se3integration.model.MoviePreference;
import wwi18b2.se3.group8.se3integration.service.MoviePreferenceService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class MoviePreferenceController {

    private MoviePreferenceService moviePreferenceService;

    public MoviePreferenceController(MoviePreferenceService moviePreferenceService){
        this.moviePreferenceService = moviePreferenceService;
    }

    @GetMapping("/preference/movie/{id}")
    public Optional<MoviePreference> getMoviePreferenceById(@PathVariable Long id){
        return moviePreferenceService.readMoviePreference(id);
    }

    @GetMapping("/preference/movie/customer/{customerId}")
    public List<MoviePreference> getMoviePreferenceByCustomerId(@PathVariable String customerId){
        return moviePreferenceService.getMoviePreferenceByCustomerId(customerId);
    }

    @GetMapping("/preference/movie/customer/{customerId}/friend/{customerId2}")
    public List<Long> getTohetherLikedMoviesByCustomerIds(@PathVariable String customerId, @PathVariable String customerId2){
        return moviePreferenceService.getTogetherLikedMoviesByCustomerIds(customerId, customerId2);
    }


    @PostMapping("/preference/movie")
    public MoviePreference postMoviePreference(@RequestBody MoviePreference moviePreference){
        return moviePreferenceService.saveNewMoviePreference(moviePreference);
    }

    @PutMapping("/preference/movie/{id}")
    public MoviePreference putMoviePreference(@RequestBody MoviePreference moviePreference, @PathVariable Long id){
        return moviePreferenceService.updateMoviePreference(moviePreference, id);
    }

    @DeleteMapping("/preference/movie/{id}")
    public void deleteMoviePreferenceById(@PathVariable Long id){
        moviePreferenceService.deleteMoviePreferenceById(id);
    }







}
