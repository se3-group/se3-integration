package wwi18b2.se3.group8.se3integration.service;

import org.springframework.stereotype.Service;
import wwi18b2.se3.group8.se3integration.model.MoviePreference;
import wwi18b2.se3.group8.se3integration.repository.MoviePreferenceRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MoviePreferenceService {

    MoviePreferenceRepository moviePreferenceRepository;

    public MoviePreferenceService(MoviePreferenceRepository moviePreferenceRepository){
        this.moviePreferenceRepository = moviePreferenceRepository;
    }

    public MoviePreference saveNewMoviePreference(MoviePreference moviePreference){
        MoviePreference savedMoviePreference = moviePreferenceRepository.findByCustomerIdAndMovieId(moviePreference.getCustomerId(), moviePreference.getMovieId());
        if(savedMoviePreference != null){
            savedMoviePreference.setLikeMovie(moviePreference.isLikeMovie());
            return moviePreferenceRepository.save(savedMoviePreference);
        }
        return moviePreferenceRepository.save(moviePreference);
    }

    public MoviePreference updateMoviePreference(MoviePreference moviePreference, Long id){
        moviePreference.setId(id);
        return moviePreferenceRepository.save(moviePreference);
    }

    public Optional<MoviePreference> readMoviePreference(Long id){

        return moviePreferenceRepository.findById(id);
    }

    public void deleteMoviePreferenceById(Long id){

        moviePreferenceRepository.deleteById(id);
    }

    public List<MoviePreference> getMoviePreferenceByCustomerId(String customerId){
        Iterable<MoviePreference> iterableMoviePreferences = moviePreferenceRepository.findAllByCustomerId(customerId);
        List<MoviePreference> moviePreferences = new ArrayList<>();
        iterableMoviePreferences.forEach(moviePreferences::add);
        return  moviePreferences;
    }

    public List<Long> getTogetherLikedMoviesByCustomerIds(String customerId1, String customerId2){
        Iterable<MoviePreference> iterableMoviePreferences1 = moviePreferenceRepository.findAllByCustomerIdAndLikeMovieTrue(customerId1);
        List<Long> likedMovies1 = new ArrayList<>();
        iterableMoviePreferences1.forEach(moviePreference -> likedMovies1.add(moviePreference.getMovieId()));

        Iterable<MoviePreference> iterableMoviePreferences2 = moviePreferenceRepository.findAllByCustomerIdAndLikeMovieTrue(customerId2);
        List<Long> likedMovies2 = new ArrayList<>();
        iterableMoviePreferences2.forEach(moviePreference -> likedMovies2.add(moviePreference.getMovieId()));

        List<Long> togetherLikedMovie = new ArrayList<>();
        likedMovies1.forEach(moviePreference -> {
            if(likedMovies2.contains(moviePreference)){
                togetherLikedMovie.add(moviePreference);
            }
        });
        return togetherLikedMovie;
    }


}
