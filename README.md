# se3-integration
Dieser Service ist dazu zuständig, alle Präferenzen von Nutzern zu Filmen zu speichern.

### initialer Start der Anwendung:
1. Sicherstellen, dass docker, maven und java 11 installiert ist
2. Datenbank starten mit dem Befehl "docker-compose up" aus dem classpath des Projektes
3. Die main-Methode aus Se3IntegrationApplication starten

